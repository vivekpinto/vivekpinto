### Hello, I'm Vivek Pinto 👋

- 🔭 I’m currently working on ML Project
- 🌱 I’m currently learning cloud computing 
- 👯 I’m looking to collaborate on Postman
- 🤔 I’m looking for help with DSA and Linux
- 💬 Ask me about Cricket
- 📫 How to reach me: [vivekpinto](https://www.linkedin.com/in/vivkepinto2001/)
- 😄 Pronouns: He/him
- ⚡ Fun fact: HP, Microsoft and Apple have one very interesting thing in common – they were all started in a garage.

<img src="https://github-readme-stats.vercel.app/api?username=vivekpinto&&show_icons=true&title_color=ffffff&icon_color=bb2acf&text_color=daf7dc&bg_color=000080">
